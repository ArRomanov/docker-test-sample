import pytest


@pytest.mark.parametrize("first_num, second_num, result", [(0, 10, 10), (-4, -8, -12), (354, 754, 1108)])
def test_numbers_addition(first_num: int, second_num: int, result: int) -> None:
    """Тест сложения двух чисел.

    :param first_num: первое число
    :param second_num: второе число
    :param result: сумма
    """
    assert first_num + second_num == result
